var express = require('express');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Nivel2 = require('../models/nivel2');

// ==========================================
// Obtener todos las niveles2
// ==========================================
app.get('/', (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Nivel2.find({})
        .skip(desde)
        .limit(5)
        .populate('usuario', 'nombre email')
        .exec(
            (err, niveles2) => {

                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando nivel2',
                        errors: err
                    });
                }

                Nivel2.count({}, (err, conteo) => {

                    res.status(200).json({
                        ok: true,
                        niveles2: niveles2,
                        total: conteo
                    });
                })

            });
});

// ==========================================
//  Obtener Nivel2 por ID
// ==========================================
app.get('/:id', (req, res) => {

    var id = req.params.id;

    Nivel2.findById(id)
        .populate('usuario', 'nombre img email')
        .exec((err, nivel2) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'Error al buscar nivel2',
                    errors: err
                });
            }

            if (!nivel2) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'El nivel2 con el id ' + id + 'no existe',
                    errors: { message: 'No existe un nivel2 con ese ID' }
                });
            }
            res.status(200).json({
                ok: true,
                nivel2: nivel2
            });
        })
})





// ==========================================
// Actualizar Nivel2
// ==========================================
app.put('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;
    var body = req.body;

    Nivel2.findById(id, (err, nivel2) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar nivel2',
                errors: err
            });
        }

        if (!nivel2) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El nivel2 con el id ' + id + ' no existe',
                errors: { message: 'No existe un nivel2 con ese ID' }
            });
        }


        nivel2.nombre = body.nombre;
        nivel2.usuario = req.usuario._id;

        nivel2.save((err, nivel2Guardado) => {

            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar nivel2',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                nivel2: nivel2Guardado
            });

        });

    });

});



// ==========================================
// Crear un nuevo nivel2
// ==========================================
app.post('/', mdAutenticacion.verificaToken, (req, res) => {

    var body = req.body;

    var nivel2 = new Nivel2({
        nombre: body.nombre,
        usuario: req.usuario._id
    });

    nivel2.save((err, nivel2Guardado) => {

        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear nivel2',
                errors: err
            });
        }

        res.status(201).json({
            ok: true,
            nivel2: nivel2Guardado
        });


    });

});


// ============================================
//   Borrar un nivel2 por el id
// ============================================
app.delete('/:id', mdAutenticacion.verificaToken, (req, res) => {

    var id = req.params.id;

    Nivel2.findByIdAndRemove(id, (err, nivel2Borrado) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error borrar nivel2',
                errors: err
            });
        }

        if (!nivel2Borrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un nivel2 con ese id',
                errors: { message: 'No existe un nivel2 con ese id' }
            });
        }

        res.status(200).json({
            ok: true,
            nivel2: nivel2Borrado
        });

    });

});


module.exports = app;