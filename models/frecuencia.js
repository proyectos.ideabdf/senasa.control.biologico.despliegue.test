var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var frecuenciaSchema = new Schema({
    fecha: { type: String, required: true },
    asistencia: { type: Boolean, required: true, default: false },
    oferta: { type: Schema.Types.ObjectId, ref: 'Oferta' },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collection: 'frecuencias' });



module.exports = mongoose.model('Frecuencia', frecuenciaSchema);