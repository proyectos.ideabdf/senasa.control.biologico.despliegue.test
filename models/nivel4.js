var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var nivel4Schema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, { collection: 'Niveles4' });



module.exports = mongoose.model('Nivel4', nivel4Schema);